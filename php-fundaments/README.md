<h1>Básico de PHP: Instalación, Fundamentos y Operadores</h1>

<h3>Carlos Eduardo Gómez</h3>

<h1>Table of Contents</h1>

- [1. Introducción](#1-introducción)
  - [Cómo aprender a programar](#cómo-aprender-a-programar)
  - [¿PHP está muerto?](#php-está-muerto)
  - [Cómo interactúa una página web con tu backend](#cómo-interactúa-una-página-web-con-tu-backend)
    - [Cliente y servidor](#cliente-y-servidor)
    - [Dominio](#dominio)
    - [Servidor físico o VPS](#servidor-físico-o-vps)
    - [Servidor web](#servidor-web)
    - [Métodos HTTP](#métodos-http)
    - [GET](#get)
    - [POST](#post)
    - [PUT/PATCH](#putpatch)
    - [DELETE](#delete)
- [2. Instalación](#2-instalación)
  - [Instalación de PHP en Windows](#instalación-de-php-en-windows)
  - [Instalación de PHP en Linux](#instalación-de-php-en-linux)
  - [Instalación de PHP en macOS](#instalación-de-php-en-macos)
  - [🐘 Instalación de PHP y Apache con Homebrew](#-instalación-de-php-y-apache-con-homebrew)
  - [🤔 ¿Cómo puedo cambiar entre versiones de PHP?](#-cómo-puedo-cambiar-entre-versiones-de-php)
  - [💻 Levantando nuestro servidor Apache](#-levantando-nuestro-servidor-apache)
- [3. Ejecutando código y archivos PHP](#3-ejecutando-código-y-archivos-php)
  - [Hackea tu miedo a la terminal](#hackea-tu-miedo-a-la-terminal)
    - [Error al iniciar docker](#error-al-iniciar-docker)
  - [Cómo ejecutar tus programas escritos con PHP](#cómo-ejecutar-tus-programas-escritos-con-php)
- [4. Aprendiendo a caminar con PHP](#4-aprendiendo-a-caminar-con-php)
  - [Sintaxis básica de PHP](#sintaxis-básica-de-php)
  - [Debugging y comentarios](#debugging-y-comentarios)
  - [Variables y constantes](#variables-y-constantes)
- [5. Tipado en PHP](#5-tipado-en-php)
  - [Tipos de datos](#tipos-de-datos)
  - [¿Qué es el casting?](#qué-es-el-casting)
      - [ejercicio:](#ejercicio)
  - [Reto: análisis de tipado en PHP](#reto-análisis-de-tipado-en-php)
- [6. Operadores en PHP](#6-operadores-en-php)
  - [Operadores lógicos: ¿qué son las tablas de verdad?](#operadores-lógicos-qué-son-las-tablas-de-verdad)
  - [Reto: análisis de operadores lógicos](#reto-análisis-de-operadores-lógicos)
  - [Operadores aritméticos](#operadores-aritméticos)
  - [Operadores relacionales](#operadores-relacionales)
  - [Otros operadores](#otros-operadores)
  - [Precedencia de operadores](#precedencia-de-operadores)
- [7. Programando con PHP](#7-programando-con-php)
  - [Tu primer programa: ¿qué hora es?](#tu-primer-programa-qué-hora-es)
  - [Solución del reto: ¿qué hora es?](#solución-del-reto-qué-hora-es)
  - [¿Quieres un Curso Básico de PHP: Arreglos,](#quieres-un-curso-básico-de-php-arreglos)

# 1. Introducción

## Cómo aprender a programar

Programar → limite es tu imaginación

Algoritmo: Serie de pasos ordenados

Programa: algoritmo en un lenguaje de programación, las computadoras puede seguir estos pasos

Lenguajes (JS, Python, Java, PHP, etc)

Por que PHP?

- Fácil de aprender y flexible
- Documentación en españo
- Bastante demandado
- Laravel y Wordpress
- Una gran parte de los sitios web lo usan

PHP

- ES MUY FACIL DE APRENDER: es muy sencillo porque ya exinten cosas hechas y solo se necesita de algoritmo e implementacion .
- ES UN LENGUAJE MUY FLEXIBLE: al ser flexible pueden ocurrir malas practicas, en este curso aprenderemos a realizarlas.
  *TIENE UNA AMPLIA DOCUMENTACION: En nuestro idioma existen muchos manuales .
  *ES MUY DEMANDADO: actualmente las empresas utilizan este lenguaje y solicitan programadores para dar mantenimiento o para crear soluciones en el backend es bien pagado y lo seguira siendo.
- TIENE A LARAVEL Y WORDPRESS: laravel ayuda a crear sistemas sencillos usando muy pocas lineas de codigo, y wordpress es una herramienta que utilizan para crear paginas web de forma sencilla al tomar plantillas plugins y hacer una pagina web en linea.
- UNA GRAN PARTE DE SITIOS WEB LA UTILIZAN : es por esa razon QUE PHP NO ESTA MUERTO

## ¿PHP está muerto?

❗ Ningún lenguaje es mejor o peor que otro. Lo importante a tener en cuenta es que los lenguajes son herramientas, y como herramientas nos ayudan a resolver problemas ☝.

💪 Cada lenguaje tiene sus fortalezas (y debilidades).

- 🐍 Python es muy bueno para todo lo que tiene que ver con Data Science.
- ⚙️ JavaScript es muy bueno para el frontend y servidores realtime.
- 🐘 PHP es muy bueno para hacer aplicaciones de backend y API

😎 Además recuerden… Freddy ama PHP 👇

![freddy.jpg](https://static.platzi.com/media/user_upload/freddy-19320c4b-d1ea-42f5-a9aa-28beb358954c.jpg)

[Facebook presenta XHP](https://volkanrivera.com/esp/2010/02/facebook-presenta-xhp-un-php-con-esteriodes/)

## Cómo interactúa una página web con tu backend

### Cliente y servidor

Toda nuestra aplicación esta guardada en un servidor, el cual entrega una copia de la misma a cada cliente que la solicite.

Además, el servidor también se encarga de responder cada solicitud del usuario.

### Dominio

El dominio es nuestra dirección en internet. Gracias a él cualquier computadora es capaz de encontrar páginas web.

### Servidor físico o VPS

Es la computadora que se encarga de guardar tu página web y mantener accesible 24/7. Se le conoce como servidor y siempre está conectado a internet.

A través de él podemos definir ciertas reglas de seguridad para nuestra página.

### Servidor web

Es un programa que corre dentro de nuestro servidor físico y se encarga de gestionar cualquier petición que llegue al mismo.

Esta petición es procesada por algún lenguaje de programación y al final devuelve una respuesta.

### Métodos HTTP

Los métodos HTTP son una forma de comunicación entre el cliente y el navegador. A través de una solicitud HTTP el cliente es capaz de pedirle al servidor que realice una acción

### GET

Este método permite solicitar información al servidor. Por ejemplo, podemos pedirle una lista de productos en el caso de que estemos haciendo un e-commerce o una lista de cursos si tenemos una pagina como Platzi.

### POST

Este método permite guardar información. Por ejemplo, podemos recabar datos del usuario desde un formulario y mandarlos a nuestro servidor para procesarlos.

podríamos guardarlos para armar una base de datos de usuarios o incluso un sistema de login.

### PUT/PATCH

Estos métodos permiten actualizar información ya guardada. Por ejemplo, podemos darle la oportunidad a un usuario de actualizar su correo electrónico o incluso cambiar su contraseña.

La diferencia es que PUT reemplaza toda la información existente y PATCH solo reemplaza lo necesario, es decir, “parcha” la información

### DELETE

Este método lo usamos para eliminar un recurso del servidor. Por ejemplo, podemos usarlo si deseamos eliminar un blogpost o un comentario.

Esto no significa que dejamos eliminarlo necesariamente dentro de nuestra base de datos, podemos hacer un “Soft delete”.

![client-server-model-svg.png](https://static.platzi.com/media/user_upload/client-server-model-svg-7190d349-45c5-4a9e-bf36-d922c3295898.jpg)

# 2. Instalación

## Instalación de PHP en Windows

[Laragon](https://platzi.com/clases/2646-php/44432-instalacion-de-php-en-windows/url)

[![img](https://www.google.com/s2/favicons?domain=https://www.apachefriends.org/es/index.html/images/favicon-18f9bd42.png)XAMPP Installers and Downloads for Apache Friends](https://www.apachefriends.org/es/index.html)

## Instalación de PHP en Linux

Agregar el repositorio de php

```sh
sudo add-apt-repository ppa:ondrej/php
```

Actualizar el repositorio del sistema

```sh
sudo apt update
```

Instalar los paquetes actualizados

```sh
sudo apt upgrade
```

Instalamos `php8.0` y `apache2` 

```sh
sudo apt install php8.0 apache2

# instalar php7.4
sudo apt install php7.4
```

Verficar las versiones instaladas de `php`

```sh
sudo dpkg --get-selections | grep php
```

Configurar `apache` con una version de `php`

```sh
sudo a2enmod php8.0

# php7.4
sudo a2enmod php7.4
```

Utilizar una version diferente de php con apache

```sh
sudo a2dismod php8.0
```

Reiniciar `apache`

```sh
systemctl restart apache2
```

Archivos de php

```sh
cd /var/www/html
```

eliminar y crear un nuevo archivo `index.php`

```sh
# Eliminar archivo
rm index.php # error, eliminarlo con root, sudo
# Crear archivo
touch index.php
```

`index.php`

```php
<?php
phpinfo();
```

configurar `php`

```sh
sudo nano /etc/php/8.0/apache/php.ini

# buscar display_errors = off, habilitar
display_errors = On
```

paquete adicional 

```sh
sudo apt install libapache2-mod-php8.0
```

`php` en termianl

```php
➜ php -a

Intercative mode enabled

php> echo "Hola madre, soy Hakcer porque instale php usando la terminal :)";

# ctl + c = exit
```

## Instalación de PHP en macOS

¿Una clase de lectura? 😫 ¡Sí! Pero no es cualquier clase de lectura, esta es la mejor clase de lectura que tendrás nunca… Bueno, en realidad solo vamos a instalar PHP en macOS, pero vamos ¡acompáñame, será divertido!

☝️ Instalar PHP en macOS es muy similar a hacerlo con Linux… Sí, tenemos MAMP y XAMPP para macOS, pero recuerda que los chicos cool lo hacemos de la forma más pro y fancy posible usando la terminal 😎.

Antes de poder instalar PHP en macOS necesitamos tener instalado Homebrew…

Home… ¿Qué?.. 🤔 ¡Sí! Homebrew, este es el manejador de paquetes de macOS. Es similar al `apt` que vimos en Linux, solo que este es para computadoras con macOS. Con este manejador simplemente debemos decirle qué queremos instalar y él solito irá e instalará el paquete que queramos.

Si aún no tienes Homebrew en tu computadora, te recomiendo tomar el [Curso de Prework: Configuración de Entorno de Desarrollo en macOS](https://platzi.com/clases/prework-macos/) en donde aprenderás a dejar tu computadora macOS 100% lista para que aprendas a programar lo que tú quieras 😎 (y aprenderás a instalar Homebrew por supuesto).

Dicho esto, ¡vamos a lo que nos interesa!

## 🐘 Instalación de PHP y Apache con Homebrew

Para empezar a instalar PHP primero debemos abrir la terminal nativa de macOS y asegurarnos de que Homebrew está instalado. Esto podemos hacerlo con el comando `brew --version`. Si te da una respuesta diciéndote qué versión de Homebrew tienes es porque todo está listo para empezar.

Primero debemos instalar una librería llamada `curl`. Esta librería es usada por PHP para descargar otros complementos necesarios. Podemos instalarla con este comando:

```sh
brew install curl
```

Con esta librería instalada, de la misma forma en la que lo hicimos con Linux, necesitaremos instalar nuestro servidor web (Apache) junto con alguna versión de nuestro lenguaje de programación. Por ejemplo PHP 8.0 que es la versión que estaremos usando en nuestro curso. En el caso de Homebrew, el paquete para instalar Apache es llamado `httpd`, por lo que con el siguiente comando deberíamos poder instalar Apache junto con PHP 8.0:

```sh
brew install httpd php@8.0
```

Esto empezará a descargar todo lo necesario para instalar Apache y PHP. Puede tomar un tiempo así que ve por un cafecito y relájate ☕️.

![Captura de pantalla 2021-11-12 a la(s) 12.34.51 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%2012.34.51%20p.%20m.-526cca21-d69f-41a8-84dc-1f4199d971a0.jpg)

De la misma forma que en Linux, en macOS también puedes instalar cualquier otra versión de PHP especificándoselo a Homebrew:

```sh
brew install php@7.4
```

☝️ Recuerda **NO** usar `sudo` cuando usas Homebrew. Esto es porque Homebrew no requiere de permisos de administrador, ya que se considera inseguro y peligroso darle todos los accesos a algún programa.

## 🤔 ¿Cómo puedo cambiar entre versiones de PHP?

Aunque ya instalamos la versión 8.0 y la versión 7.4 de PHP, verás que si corres el comando `php --version` te va a decir que tienes la versión 7.1… ¿What?

![Captura de pantalla 2021-11-12 a la(s) 1.09.25 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%201.09.25%20p.%20m.-63c3f8c6-af35-43c6-9a45-651aef55b56a.jpg)

Bien, esto sucede porque por defecto PHP ya viene instalado en macOS, pero en su versión 7.1. Debemos decirle a macOS que queremos usar otra versión de PHP. Para cambiar entre versiones de PHP primero debemos decirle a Homebrew que deje de usar la versión actual, después le decimos cuál es la versión que quiero usar ahora y por último agregamos esa versión a nuestro `PATH`, es decir, con estos comandos debería bastar. Simplemente, debes cambiar el `8.0` por la versión que quieras usar:

```sh
brew unlink php
brew link --force php@8.0
export PATH="/usr/local/opt/php@8.0/bin:$PATH"  
export PATH="/usr/local/opt/php@8.0/sbin:$PATH"
```

Ahora si vuelves a ejecutar el comando `php --version` verás que ya tenemos habilitada la versión 8.0 😎

![Captura de pantalla 2021-11-12 a la(s) 2.13.53 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%202.13.53%20p.%20m.-49f6e0f4-bea1-4aad-9e90-431d50b846f1.jpg)

## 💻 Levantando nuestro servidor Apache

Como ya vimos, una página web requiere de un servidor web. En este caso Apache es el servidor web que nos ayudará a poner en línea nuestra página web, sin embargo, necesitamos hacer unas configuraciones antes. Para ello en tu terminal escribe el siguiente comando:

```sh
sudo nano /usr/local/etc/httpd/httpd.conf
```

Esto nos abrirá este editor de texto en la terminal, no te preocupes por todas esas cosas raras que ves ahí, simplemente vamos a hacer unos cambios muy chiquititos.

![Captura de pantalla 2021-11-12 a la(s) 2.29.27 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%202.29.27%20p.%20m.-66c854ca-d9c7-41b8-b77c-9bb45f91da9b.jpg)

En primera, debemos cambiar el puerto de Apache, ya que Homebrew establece por defecto el puerto 8080, sin embargo, las páginas web suelen mostrarse en el puerto 80. Para cambiar esto, presiona las teclas `ctrl + w` y se te abrirá un cuadro de búsqueda, ahí escribe “Listen 8080” y presiona “enter”. Esto te llevará a una línea en donde se especifica qué puerto usar, así que simplemente cambia ese 8080 que está escrito por 80, eso es todo 😄.

![Captura de pantalla 2021-11-12 a la(s) 2.44.41 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%202.44.41%20p.%20m.-ce336ec3-1fd3-44bf-b36f-92385bc4f277.jpg)

Con esto ya cambiamos el puerto. Ahora debemos enlazar PHP con Apache, para ello basta con que bajes un poquito presionando la tecla con la flecha hacia abajo y te toparás con esta lista enorme de LoadModule:

![Captura de pantalla 2021-11-12 a la(s) 2.52.51 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%202.52.51%20p.%20m.-4dd476dc-2910-46b0-b572-80e4a216f21c.jpg)

Simplemente, debes agregar una nueva línea en donde tú quieras con el siguiente código:

```sh
LoadModule php_module /usr/local/opt/php/lib/httpd/modules/libphp.so
```

Ahora simplemente vamos a agregar unas últimas configuraciones. Vuelve a presionar `Ctrl + W`⁣, ya que haremos otra búsqueda y escribe “DirectoryIndex”, presiona “enter” y te debe de llevar hacia este apartado:

![Captura de pantalla 2021-11-12 a la(s) 2.57.52 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%202.57.52%20p.%20m.-1c87ddd7-0b06-4a99-9cc7-f25ef315b90e.jpg)

Aquí lo único que debes hacer es cambiar la línea que dice `DirectoryIndex index.html` por esta línea:

```sh
DirectoryIndex index.php index.html
```

Debería quedarte así:

![Captura de pantalla 2021-11-12 a la(s) 2.59.48 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%202.59.48%20p.%20m.-d47e39d6-e724-4120-ae4d-d2d05d2f7ca0.jpg)

Por último al final de tu archivo agrega lo siguiente:

```sh
	SetHandler application/x-httpd-php
```

¡Y eso es todo! 🤞Lo prometo… Ahora para que todo quede listo y guardado, simplemente presiona `ctrl + o`, te pedirá confirmación así que únicamente debes presionar “enter” y por último puedes salir presionando `ctrl + x`.

Lo único que queda hacer es reiniciar nuestros servicios para que los cambios se apliquen 😎. Para ello simplemente ejecutamos estos dos comandos:

```sh
brew services restart php
sudo apachectl -k start
```

Y si entramos a `localhost` deberías ver este mensaje:

![Captura de pantalla 2021-11-12 a la(s) 3.29.24 p. m..png](https://static.platzi.com/media/user_upload/Captura%20de%20pantalla%202021-11-12%20a%20la%28s%29%203.29.24%20p.%20m.-d819ec8f-51f4-416b-b475-452a0c73be1e.jpg)

¡Y con eso ya tienes PHP y Apache corriendo! Ahora simplemente puedes empezar a crear todas tus carpetas y archivos PHP dentro de la carpeta `/usr/local/var/www/`

# 3. Ejecutando código y archivos PHP

## Hackea tu miedo a la terminal

**Opciones de la línea de comandos**

- -a Se ejecuta interactivamente.

- -c <ruta>|<fichero> Busca el fichero php.ini en este directorio.

- -n No se usará el fichero php.ini.

- -d foo[=bar] Define la entrada INI de foo con el valor ‘bar’

- -e Generate información extendida para el depurador/perfilador.

- -f <fichero> Analiza y ejecuta el <fichero>.

- -h Esta ayuda.

- -i Información de PHP.

- -l Solamente revisa la sintáxis (lint).

- -m Muestra lo compilado en módulos.

- -r <code> Ejecuta el <código> PHP sin utilizar las etiquetas del script

###  `docker-compose`

**Correr el `docker-compse`**

```sh
docker-compose up 
```

**Ingresamos a `bash` en `docker`**

```sh
docker-compose exec php-fpm bash
```

**Ejecutamos** 

```sh
php -a
```

**codigo**

```hs
php > echo "Holi, estoy aprendiendo PHP";

# salir 
exit
```

**File `holi.php`**

```sh
# directorio principal
├── docker-php
│   ├── default.conf
│   ├── docker-compose.yml
│   ├── README.md
│   └── src
│       ├── holi.php
└──     └── index.php



# Dentro de la carpte src, crear el archivo holi.php
```

`holi.php`

```php
<?
echo "Esto, esta construido en php";
```

`docker-compose`

```sh
# Ingresamos a bash de docker-compse
docker-compose exec php-fpm bash
```

Corremos nuestro archivo

```sh
chronos@php# php holi.php
Esto, esta construido en php
```

### Error al iniciar docker

```sh
Attaching to docker-php-web-1
Error response from daemon: driver failed programming external connectivity on endpoint docker-php-web-1
...
iptables: No chain/target/match by that name.
 (exit status 1))
```

## Comandos para reiniciar docker

```sh
sudo iptables -t filter -F


sudo iptables -t filter -X


systemctl restart docker
```



## Cómo ejecutar tus programas escritos con PHP

> "La sintaxis es la parte de la gram&aacute;tica que estudia las reglas y principios que gobiernan la combinatoria de contituyentes sit&aacute;cticos y la formaci&oacute;n de unidades superiores a estos, como los sintagmas y las oraciones gramaticas."
>
> > So las reglas que dictan c&oacute;mo se debe estructurar un lenguaje para que tenga sentido lo que decimos.
> >
> > ❌ Yo quere taco carne mucha.
> >
> > 🎉 Yo quiero un taco con mucha carne.

Linux, directorio `php`

```sh
cd /var/www/html
```

Permiso `html`

```sh
sudo chmod 777 html
```

`wsl` iniciar el servidor

```sh
sudo /etc/init.d/apache2 start
```

# 4. Aprendiendo a caminar con PHP

## Sintaxis básica de PHP

Usando puntos podemos concatenar Strings con variables u operaciones matemáticas.

```
$nombre = "Pepito";

$apellido = "Grillo";


echo "No, no. Yo me llamo " . $nombre . $apellido.  "\n";

echo "El resultado de  4 x % es " . (4 * 5) . "\n";
```

También si usamos comillas dobles podemos poner directamente las variables en el string y PHP las interpretará correctamente quedando el código mucho más legible.

```php
$nombre = "Pepito";

$apellido = "Grillo";

echo "Me llamo $nombre $apellido \n";
```

Con \n añadimos un salto de linea sin embargo si ejecutamos el código para ser visto en un navegador \n no añadirá el salto de linea y tendremos que usar la etiqueta de html `<br>`

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Etiquetas de PHP - Manual](https://www.php.net/manual/es/language.basic-syntax.phptags.php)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Salir de HTML - Manual](https://www.php.net/manual/es/language.basic-syntax.phpmode.php)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Separación de instrucciones - Manual](https://www.php.net/manual/es/language.basic-syntax.instruction-separation.php)

## Debugging y comentarios

- var_dump: nos permite inspeccionar la variable y nos da información acerca de ella. Por ejemplo, en un array nos dice el número de elementos del array y el tipo que es cada elemento.

  ```php
  $personas = [
      "Carlos" => 22,
      "Mr. Michi" => 15,
      "Juan" => 65
  ];
  
  //
  var_dump( $personas );
  ```

  Salida:

  ```php
  array(3) {
  ["Carlos"]=>
  int(22)
  ["Mr. Michi"]=>
  int(15)
  ["Juan"]=>
  int(65)
  }
  ```

- print_r: nos imprime la variable de una forma más limpia y con menos informacion.

  ```php
  $personas = [
      "Carlos" => 22,
      "Mr. Michi" => 15,
      "Juan" => 65
  ];
  
  //
  print_r( $personas );
  ```

  Salida:

  ```php
  Array
  (
      [Carlos] => 22
      [Mr. Michi] => 15
      [Juan] => 65
  )
  ```

- // y # : nos sirve para dejar comentarios de una linea.

  ```php
  // Comentario de una linea
  # Otra forma de hace un comentario de una linea
  ```

- /* comentario de varias lineas */: de esta forma podemos dejar comentarios de varias lineas.

  ```php
  /* Esto es un 
  comentario multilinea */
  ```

## Variables y constantes

- Una varible empieza simpre con un signo de $ y podemos cambiar su valor:

  ```php
  $numero_1 = 8;
  $numero_2 = 7;
  
  echo $numero_1 + $numero2;
  ```

- Una constante no se puede cambiar su valor, como buena practica se crea en mayuscula y se define de la siguiente forma:

  ```php
  define("NUMERO_PI", 3.14);
  
  echo NUMERO_PI;
  ```

> A partir de la version 5.3.0 para definir una constante se puede utilizar la palabra CONST seguido de el nombre de la constante, es su uso mas comun en la actualidad.

Las variables y las constantes son estructuras que guardan valores necesarios para el funcionamiento del programa.

Sin embargo, las variables se declaran anteponiendo el signo peso (`$`) al nombre de estas, y se inicializan entregando un valor luego de poner el operador de asignación `=`.

```PHP
$variable_1 = 99;
$variable_2 = 15;
echo $variable_1 + $variable_2;  // 114
```

Las variables pueden cambiar su valor durante el tiempo de ejecución del programa. Por ejemplo, pasando de guardar un número a guardar una cadena de caracteres.

```PHP
$variable_3 = 50;
echo $variable_3;  // 50
$variable_3 = "Andrés";
echo $variable_3;  // 'Andrés'
```

Las constantes se declaran con la función `define()`; esta función recibe por argumento dos valores separados por una coma (`,`), siendo el primero el nombre de la constante encerrado entre comillas dobles, y el segundo, el valor de esta. Por convención se espera que el nombre sea en mayúsculas.

```PHP
define("NUMERO_E", 2.7183);
echo NUMERO_E;  // 2.7183
```

Para usar las constantes, no hay necesidad de anteponer el signo peso (`$`).

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Conceptos básicos - Manual](https://www.php.net/manual/es/language.variables.basics.php)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Constants - Manual](https://www.php.net/manual/en/language.constants.php)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Constantes predefinidas - Manual](https://www.php.net/manual/es/reserved.constants.php)

# 5. Tipado en PHP

## Tipos de datos

En programación tenemos varios tipos de datos, los más conocidos son:

- Numéricos:
  - Integer: Número sin decimales.
  - Float: Número con punto flotante o punto decimal.
  - Double: Decimales con valores más precisos, con más decimales que float.
- Cadenas ded caracteres:
  - Chart: Un solo una letra o un simbolo.
  - String: Una cadena de caracteres.
- Booleanos:
  - Bool: Verdadero o falso.
- Sin valor:
  - Null: No hay valor.
  - Undefined: Hay una variable pero no tiene ningun valor.

PHP tiene un tipado débil, no necesita que se defina de forma explicita el tipo de dato ya que lo deduce por si mismo.

PHP es capaz de convertir de un tipo a otro de forma automática. Por ejemplo si sumas un número con un string PHP evaluará ambos como números.

```php
/// Aunque 25 sea un string el resultado será 30.
print_r("25"+5);
```

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Tipos - Manual](https://www.php.net/manual/es/language.types.php)

## ¿Qué es el casting?

#### ejercicio:

```php
//Fácil

$nombre = "Carlos";
$apellido = "Gómez";
$edad = 18;
$aprobado = true;

//Medio

$promedio = (8 + 9.5 + 9 + 10 + 8) / 5;
$nombre_completo = $nombre . " " . $apellido;
$presento_examen = (bool) 1;

//Avanzado

$numero_preguntas = 5 + "5";
$numero_respuestas = "5" + 5;
$promedio_maximo = $numero_respuestas / 1.0;
$michis = 3 + "5 michis";
```

El *Casting* es la manera de indicar al interprete de PHP para forzar el cambio de un tipo de dato a otro deseado. Se puede acceder a esta utilidad anteponiendo el *tipo de dato* entre paréntesis antes de un valor o una variable al momento de la asignación o inicialización.

Las siguientes definiciones ayudan a forzar el cambio de tipos en PHP:

- `(array)` forzado al tipo arreglo
- `(bool)` forzado al tipo booleano
- `(boolean)` forzado al tipo booleano
- `(double)` forzado al tipo ‘punto flotante’
- `(float)` forzado al tipo ‘punto flotante’
- `(int)` forzado al tipo entero
- `(integer)` forzado al tipo entero
- `(object)` forzado al tipo objeto
- `(string)` forzado al tipo ‘cadena de texto’

A continuación, se muestra un par de ejemplos de lo mencionado:

```php
$var_3 = "5";  // string(1) "5"
$var_4 = (int) $var_3;  // int(5)
$flag = 1;  // int(1)
$flag = (bool) $flag;  // bool(true)
```

![analisis-de-tipos.png](https://static.platzi.com/media/user_upload/analisis-de-tipos-62ec2408-972d-4887-8a5b-713236ec9aa0.jpg)

## Reto: análisis de tipado en PHP

![](https://static.platzi.com/media/public/uploads/reto-analisis-de-tipado-en-php_3a0ffca8-89c9-41a8-9fce-553202c93374.png)

# 6. Operadores en PHP

## Operadores lógicos: ¿qué son las tablas de verdad?

Son los operadores que nos ayudan a combinar dos o mas afirmaciones para definir si una oración es cierta o falsa. Su uso esta basado en tablas de verdad.

#### AND (y)

Se usa para verificar si dos afirmaciones son ciertas, entonces la oración completa es cierta. Si una de ellas es falsa, entonces, la oración completa es falsa.

true AND true = True

false AND true = False

true AND false = False

false AND false = False

Se escribe así:

- $valor_1 and $valor_2
- $valor_1 && $valor_2

#### OR (o)

Si una de las 2 afirmaciones es cierta, entonces la oración completa es cierta. Si solo una de ellas es falsa, entonces, la oración completa es cierta.

true OR true = True

false OR true = True

true OR false = True

false OR false = False

Se escribe así:

- $valor_1 or $valor_2
- $valor_1 || $valor_2

NOT (no)

Se usa para invertir el significado de una oración

NOT True ⇒ False

NOT False ⇒ True

```php
<?php
$es_michi_grande = true;
$le_gusta_comer = true;
$sabe_volar = false;
$tiene_2_patas = false;

//AND
var_dump($es_michi_grande && $le_gusta_comer);
bool(true)

//OR
var_dump($es_michi_grande || $sabe_volar);
bool(true)

//OR
var_dump($sabe_volar || $tiene_2_patas);
bool(false)

//NOT
var_dump(!$le_gusta_comer);
bool(false)

//NOT+AND
var_dump(!$le_gusta_comer && $es_michi_grande);
bool(false)

echo ("\n");

```

Los *Operadores Lógicos* son elementos usados para la evaluación de
*Expresiones*, confirmando si estas son verdaderas o falsas. Una Expresión
consta de al menos dos *Afirmaciones*, como podría ser *«Los seres humanos no
pueden vivir sin oxígeno»* o *«One-Punch Man tiene una frondosa cabellera»*.
Una *Afirmación* no es mas que una oración que puede contener una verdad o una
falsedad.

La evaluación de expresiones se pueden reducir a las *tablas de verdad*; las
cuales operan sobre expresiones de dos afirmaciones. PHP usa los operadores
`and` y `or` para las tablas de verdad ‘y’ y ‘o’ respectivamente.

#### *Tabla de verdad ‘y’ (`and`)*

Use el operador `and` para comprobar, sí ambas afirmaciones en una expresión
son verdad toda la expresión es verdad, pero sí una de las afirmaciones es
falsa toda la expresión es falsa.

| Valor   | Operador | Valor   | Resultado |
| ------- | -------- | ------- | --------- |
| `true`  | `and`    | `true`  | `true`    |
| `true`  | `and`    | `false` | `false`   |
| `false` | `and`    | `true`  | `false`   |
| `false` | `and`    | `false` | `false`   |

#### *Tabla de verdad ‘o’ (`or`)*

Use el operador `or` para comprobar, sí al menos una de las afirmaciones en una
expresión es verdad toda la expresión es verdad, pero sí ambas afirmaciones son
falsas toda la expresión es falsa.

| Valor   | Operador | Valor   | Resultado |
| ------- | -------- | ------- | --------- |
| `true`  | `or`     | `true`  | `true`    |
| `true`  | `or`     | `false` | `true`    |
| `false` | `or`     | `true`  | `true`    |
| `false` | `or`     | `false` | `false`   |

PHP también soporta los operadores `&&` y `||` para las evaluaciones *y* y *o*
respectivamente.

#### *Tabla de verdad ‘y’ (`&&`)*

| Valor   | Operador | Valor   | Resultado |
| ------- | -------- | ------- | --------- |
| `true`  | `&&`     | `true`  | `true`    |
| `true`  | `&&`     | `false` | `false`   |
| `false` | `&&`     | `true`  | `false`   |
| `false` | `&&`     | `false` | `false`   |

#### *Tabla de verdad ‘o’ (`||`)*

| Valor   | Operador | Valor   | Resultado |
| ------- | -------- | ------- | --------- |
| `true`  | `||`     | `true`  | `true`    |
| `true`  | `||`     | `false` | `true`    |
| `false` | `||`     | `true`  | `true`    |
| `false` | `||`     | `false` | `false`   |

#### *Operador de negación (`not`)*

Para negar o invertir el valor de una afirmación se usa el operador ‘no’ (`!`).

| Operador | Valor   | Resultado |
| -------- | ------- | --------- |
| `!`      | `true`  | `false`   |
| `!`      | `false` | `true`    |

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Operadores lógicos - Manual](https://www.php.net/manual/es/language.operators.logical.php)

[![img](https://www.google.com/s2/favicons?domain=https://www.youtube.com/s/desktop/310f846f/img/favicon.ico)Qué son tablas de verdad y compuertas lógicas | PlatziLive - YouTube](https://www.youtube.com/watch?v=Pfyuv5ZnNNw)

## Reto: análisis de operadores lógicos

`reto.php`

```php
$es_michi_grande = true;
$le_gusta_comer = true;
$sabe_volar = false;
$tiene_2_patas = false;

$es_michi_grande && $le_gusta_comer;  // bool(true)

$es_michi_grande || $sabe_volar;  // bool(true)

$sabe_volar || $tiene_2_patas;  // bool(false)

!$le_gusta_comer;  // bool(false)

!$le_gusta_comer || $es_michi_grande;  // bool(true)
```



## Operadores aritméticos

- Adición ⇒ +
- Sustracción ⇒ -
- Multiplicación ⇒ *
- División ⇒ /
- Modulo ⇒ % ⇒ Se usa para conocer el residuo de una división ⇒ $a % $b
- Potenciación ⇒ ** ⇒ $a ** $b
- Identidad ⇒ Sirve para convertir un **string** a un **int** o **float**, según sea el caso ⇒ + ⇒ +$a
- Negación ⇒ Convierte un numero positivo a negativo ⇒ -$a

![aritmeticos.PNG](https://static.platzi.com/media/user_upload/aritmeticos-b6a4c8e9-4081-4579-8e36-5cddb278a7ed.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Operadores aritméticos - Manual](https://www.php.net/manual/es/language.operators.arithmetic.php)

## Operadores relacionales

#### Igual, ==

```php
No compara tipos, solo valores

Se denota con   ==

$a == $b
```

#### Idéntico, ===

```php
Compara tipos y valores

Se denota con ===
```

#### Diferentes, ! =, ! ==

```php
! = , no compara tipos,

! ==, compara tipos y valores,
```

#### Menor que, <

```php
$a < $b
```

#### Mayor que, >

```php
$a > $b
```

#### Mayor o Igual que, > =

```php
$a ≥ $b
```

#### Menor o Igual que, < =

```php
$a < = $b
```

#### Operador de Nave Espacial, < = >

```php
<?php
	echo 2 <=> 1;
	echo "\n";

echo 2 <=> 1; // 1
echo 1 <=> 1; // 0
echo -50 <=> 1 //
```

- Si el numero de la izquierda es mayor el numero de la derecha, el resultado será 1
- Si el numero de la derecha es igual que el numero de la izquierda , el resultado será 0
- Si el numero de la izquierda es menor que el numero de la derecha, el resultado será 1

#### Fusión de Null, ??

```php
$edad_pepito = 23;

echo $edad_juanito ?? edad_pepito; 
// Si la edad de Juanito no esta definida, usa la edad de pepito

echo "\n";
```

![Nave.PNG](https://static.platzi.com/media/user_upload/Nave-51cbbaae-8041-49f0-8024-aa54b79c139b.jpg)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Operadores de comparación - Manual](https://www.php.net/manual/es/language.operators.comparison.php)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Operadores de incremento/decremento - Manual](https://www.php.net/manual/es/language.operators.increment.php)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Operadores para strings - Manual](https://www.php.net/manual/es/language.operators.string.php)

[![img](https://www.google.com/s2/favicons?domain=https://www.php.net/favicon.ico)PHP: Operadores de asignación - Manual](https://www.php.net/manual/es/language.operators.assignment.php)

## Otros operadores

#### Operador de Asignación, =

#### Operador de Incremento, +=

```php
$contador = 1;
$contador = $ contador + 1;
$contador += 1;
$contador++; /Sirve para incrementar de 1 en 1
```

#### Operador, -=

Resta el numero que pongas a un lado

#### Operador, *=

Multiplica por el numero que pongas a un lado

#### Operador, /=

Divide entre el numero que pongas a un lado

#### Operador de Concatenación, .=

```php
$nombre = "Carlos";
$nombre .= " " . "Santana";
echo $nombre;
     Salida: Carlos Santana
```

## Precedencia de operadores

La Precedencia de Operadores nos sirve para decidir qué pasará?, en qué orden?, cuando estamos programando. Para que funcione primero incializamos la variable y luego le asignas la funcion que debe realizar, es decir, por ejemplo, primero le das le valor de cero a la variable y luego le dices que le sume 1 cada vez que haga un ciclo.

> La precedencia de un operador indica qué tan “estrechamente” se unen dos expresiones juntas.

`precedencia.php`

```php
<?php
$michis_4_patas = true;
$michis_programan_con_PHP = false;

$resultado = ($michis_4_patas and $michis_programan_con_PHP);

var_dump( $resultado );

echo "\n";
```

# 7. Programando con PHP

## Tu primer programa: ¿qué hora es?

 solucionar el reto:

```php
<?php

$horas = readline("Ingresa la hora: ");
$minutos = readline("Ingresa los minutos: ");
$segundos = readline("Ingresa los segundos: ");

$horas = (int) ($horas * 3600);
$minutos = (int) ($minutos * 60);
$result = $horas + $minutos + $segundos;

echo "Son $result segundos. \n";
```

## Solución del reto: ¿qué hora es?

solucion del reto

```php
<?php

$horas = readline("Por fovar, ingresa el numero de horas: ");
$minutos = readline("Por favor, ingresa los minutos: ");
$segundos = readline("Por favor, ingresa los segundos: ");

$tiempo_en_segundos = ($horas * 60 * 60) + ($minutos * 60) + $segundos;

echo "Este tiempo equivale a $tiempo_en_segundos segundos. \n";
```

## ¿Quieres un Curso Básico de PHP: Arreglos,

<h2>Nunca pares de Aprender</h2>

