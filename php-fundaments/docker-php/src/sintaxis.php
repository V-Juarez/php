<?php

// \n termianl
// <br/> navegador
echo "Hola, madre. Estoy programando con phpcito <br>";

// concatenacion
$nombre = "Carlos";
$apellido = "Santana";

// echo "No, no. Yo me llamo " . $nombre . " " . $apellido . "<br>";
echo "NO, no. Yo me llamo $nombre $apellido <br>";

// "" comillas dobles, lee muchas acciones
// '' No permite lectura de mas funciones

echo "El resultado de 4 x 5 es " . (4 * 5) . "<br>";