<?php

abstract class Base {
  protected $name;

  private function getClassName() {
    return get_called_class();
  }

  public function login() {
    return "MI nombre es $this->name desde la clase {$this->getClassName}";
  }
}

class Admin extends Base {
  public function _construct($name)
}